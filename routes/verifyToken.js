import jwt from 'jsonwebtoken'

const verifyToken = (req, res, next) => {
    const authHeader = req.headers.token
    if(authHeader){
        const token = authHeader.split(" ")[1];
        jwt.verify(token, process.env.JWT_SEC, (err, user) => {
            if(err) res.status(403).json({message: "Token is not valid!"});
            req.user = user
            next()
        })
    }else{
        return res.status(401).json({message: "You are not authenticated"})
    }
};

const verifyTokenAndAuthorization = (req, res, next) => {
    verifyToken(req, res, () => {
        console.log(req.user)
        if(req.user.id === req.params.id || req.user.isAdmin || req.user.isAdminAssistant) {
            
            next();
        } else {
            res.status(403).json({message: "Action's Prohibited"})
        }
    });
};

const verifyTokenAndAdmin = (req, res, next) => {
    verifyToken(req, res, () => {
        console.log(req.user)
        if(req.user.isAdmin) {
            
            next();
        } else {
            res.status(403).json({message: "Action's Prohibited"})
        }
    });
};

const verifyTokenAndAdminAssitant = (req, res, next) => {
    verifyToken(req, res, () => {
        console.log(req.user)
        if(req.user.isAdmin || req.user.isAdminAssistant) {
            
            next();
        } else {
            res.status(403).json({message: "Action's Prohibited"})
        }
    });
};

export { verifyToken, verifyTokenAndAuthorization, verifyTokenAndAdmin, verifyTokenAndAdminAssitant };