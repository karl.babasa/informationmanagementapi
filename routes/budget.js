import express from 'express'
const router = express.Router();

import { verifyTokenAndAdmin, verifyTokenAndAdminAssitant } from './verifyToken.js';
import { createBudget, getBudget, getBudgets, editBudget, activateBudget, deactivateBudget, deleteBudget } from '../controllers/budget.js'

//POST
router.post("/add", verifyTokenAndAdminAssitant, createBudget);

//GET
router.get('/:id', verifyTokenAndAdminAssitant, getBudget);

router.get('/', verifyTokenAndAdminAssitant, getBudgets);

//PUT
router.put("/:id", verifyTokenAndAdminAssitant, editBudget)

router.put("/:id/activate", verifyTokenAndAdminAssitant, activateBudget)

router.put("/:id/deactivate", verifyTokenAndAdminAssitant, deactivateBudget)

//DELETE
router.delete("/:id", verifyTokenAndAdmin, deleteBudget)

export default router;