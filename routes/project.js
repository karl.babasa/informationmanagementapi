import express from 'express'
const router = express.Router();

import { verifyTokenAndAdmin, verifyTokenAndAdminAssitant } from './verifyToken.js';
import { createProject, getProject, getProjects, editProject, activateProject, deactivateProject, deleteProject } from '../controllers/project.js'

//POST
router.post("/add", verifyTokenAndAdminAssitant, createProject);

//GET
router.get('/:id', verifyTokenAndAdminAssitant, getProject);

router.get('/', verifyTokenAndAdminAssitant, getProjects);

//PUT
router.put("/:id", verifyTokenAndAdminAssitant, editProject)

router.put("/:id/activate", verifyTokenAndAdminAssitant, activateProject)

router.put("/:id/deactivate", verifyTokenAndAdminAssitant, deactivateProject)

//DELETE
router.delete("/:id", verifyTokenAndAdmin, deleteProject)

export default router;