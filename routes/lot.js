import express from 'express'
const router = express.Router();

import { verifyTokenAndAdmin, verifyTokenAndAdminAssitant } from './verifyToken.js';
import { createLot, getLot, getLots, editLot, activateLot, deactivateLot, deleteLot } from '../controllers/lot.js'

//POST
router.post("/add", verifyTokenAndAdminAssitant, createLot);

//GET
router.get('/:id', verifyTokenAndAdminAssitant, getLot);

router.get('/', verifyTokenAndAdminAssitant, getLots);

//PUT
router.put("/:id", verifyTokenAndAdminAssitant, editLot)

router.put("/:id/activate", verifyTokenAndAdminAssitant, activateLot)

router.put("/:id/deactivate", verifyTokenAndAdminAssitant, deactivateLot)

//DELETE
router.delete("/:id", verifyTokenAndAdmin, deleteLot)

export default router;