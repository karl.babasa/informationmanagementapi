import express from 'express'
const router = express.Router();

import { createUser, loginUser } from '../controllers/auth.js'

//REGISTER
router.post("/register", createUser);

//LOGIN
router.post("/login", loginUser);

export default router;