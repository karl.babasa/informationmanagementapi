import express from 'express'
const router = express.Router();

import { verifyTokenAndAdmin, verifyTokenAndAdminAssitant } from './verifyToken.js';
import { createTitleOwner, getTitleOwner, getTitleOwners, editTitleOwner, activateTitleOwner, deactivateTitleOwner, deleteTitleOwner } from '../controllers/titleOwner.js'

//POST
router.post("/add", verifyTokenAndAdminAssitant, createTitleOwner);

//GET
router.get('/:id', verifyTokenAndAdminAssitant, getTitleOwner);

router.get('/', verifyTokenAndAdminAssitant, getTitleOwners);

//PUT
router.put("/:id", verifyTokenAndAdminAssitant, editTitleOwner)

router.put("/:id/activate", verifyTokenAndAdminAssitant, activateTitleOwner);

router.put("/:id/deactivate", verifyTokenAndAdminAssitant, deactivateTitleOwner);

//DELETE
router.delete("/:id", verifyTokenAndAdmin, deleteTitleOwner);

export default router;