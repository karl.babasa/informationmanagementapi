import express from 'express'
const router = express.Router();

import { verifyToken, verifyTokenAndAuthorization, verifyTokenAndAdmin, verifyTokenAndAdminAssitant } from './verifyToken.js';
import { createPap, getPap, getPaps, editPap, activatePap, deactivatePap, deletePap } from '../controllers/pap.js'

//POST
router.post("/add", verifyTokenAndAdminAssitant, createPap);

//GET
router.get("/:id", verifyTokenAndAdminAssitant, getPap);

router.get("/", verifyTokenAndAdminAssitant, getPaps);

//PUT
router.put("/:id", verifyTokenAndAdminAssitant, editPap);

router.put("/:id/activate", verifyTokenAndAdminAssitant, activatePap);

router.put("/:id/deactivate", verifyTokenAndAdminAssitant, deactivatePap);

router.delete("/:id", verifyTokenAndAdminAssitant, deletePap);

export default router;