import express from 'express'
const router = express.Router();

import { verifyTokenAndAdmin, verifyTokenAndAdminAssitant } from './verifyToken.js';
import { createTitle, getTitle, getTitles, editTitle, activateTitle, deactivateTitle, deleteTitle } from '../controllers/title.js'

//POST
router.post("/add", verifyTokenAndAdminAssitant, createTitle);

//GET
router.get('/:id', verifyTokenAndAdminAssitant, getTitle);

router.get('/', verifyTokenAndAdminAssitant, getTitles);

//PUT
router.put("/:id", verifyTokenAndAdminAssitant, editTitle)

router.put("/:id/activate", verifyTokenAndAdminAssitant, activateTitle)

router.put("/:id/deactivate", verifyTokenAndAdminAssitant, deactivateTitle)

//DELETE
router.delete("/:id", verifyTokenAndAdmin, deleteTitle)

export default router;