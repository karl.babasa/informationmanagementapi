import express from 'express'
const router = express.Router();

import { verifyToken, verifyTokenAndAuthorization, verifyTokenAndAdmin } from './verifyToken.js';
import { editUser, deleteUser, getUser, getUsers, activateUser, deactivateUser } from '../controllers/user.js'


//PUT
router.put("/:id", verifyTokenAndAuthorization, editUser);

//DELETE
router.delete("/:id", verifyTokenAndAuthorization, deleteUser)

//get user
router.get("/:id", verifyTokenAndAdmin, getUser)

//get all users
router.get("/", verifyTokenAndAdmin, getUsers)

//activate account
router.put("/:id/activate", verifyTokenAndAdmin, activateUser)

//deactivate account
router.put("/:id/deactivate", verifyTokenAndAdmin, deactivateUser)

export default router;