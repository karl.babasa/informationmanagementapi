import express from 'express'
const router = express.Router();

import { verifyTokenAndAdmin, verifyTokenAndAdminAssitant } from './verifyToken.js';
import { createCheque, getCheque, getCheques, editCheque, activateCheque, deactivateCheque, deleteCheque } from '../controllers/cheque.js'

//POST
router.post("/add", verifyTokenAndAdminAssitant, createCheque);

//GET
router.get('/:id', verifyTokenAndAdminAssitant, getCheque);

router.get('/', verifyTokenAndAdminAssitant, getCheques);

//PUT
router.put("/:id", verifyTokenAndAdminAssitant, editCheque)

router.put("/:id/activate", verifyTokenAndAdminAssitant, activateCheque)

router.put("/:id/deactivate", verifyTokenAndAdminAssitant, deactivateCheque)

//DELETE
router.delete("/:id", verifyTokenAndAdmin, deleteCheque)

export default router;