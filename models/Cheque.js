//const mongoose = require("mongoose");
import mongoose from "mongoose";

const chequeSchema = new mongoose.Schema({
    chequeType: {
        type: String,
        required: [true, "Cheque Type is Required"]
    },
    chequeNumber: {
        type: String,
        required: [true, "Cheque Number is Required"]
    },
    chequeAmount: {
        type: Number,
        required: [true, "Cheque Amount is Required"]
    },
    dateCreated: {
        type: String,
        required: [true, "Cheque Date Created is Required"]
    },
    dateReleased: {
        type: String
    },
    chequeName: {
        type: String,
        required: [true, "Cheque Name is Required"]
    },
    chequeStatus: {
        type: String,
        required: [true, "Cheque Status is Required"]
    },
    chequeRemark: {
        type: String
    },
    lotId: {
        type: String
    },
    isActive: {
        type: Boolean,
        default: true
    }
});


const Cheque = mongoose.model("Cheque", chequeSchema);
export default Cheque;