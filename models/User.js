//const mongoose = require("mongoose");
import mongoose from "mongoose";

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: [true, "Username is Required"],
        unique: [true, "Username already exist"]
    },
    password: {
        type: String,
        required: [true, "Password is Required"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    isAdminAssistant: {
        type: Boolean,
        default: false
    },
    firstName: {
        type: String,
        required: [true, "First Name is Required"]
    },
    middleName: {
        type: String,
    },
    lastName: {
        type: String,
        required: [true, "Last Name is Required"]
    },
    extensionName: {
        type: String
    }
});

const User = mongoose.model("User", userSchema);
export default User;
