import mongoose from "mongoose";

const titleOwnerSchema = new mongoose.Schema({
    
    firstName: {
         type: String
    },
    middleName: {
        type: String
    },
    lastName: {
        type: String
    },
    civilStatus: {
        type: String
    },
    spsFirstName: {
        type: String
    },
    spsMiddleName: {
       type: String
    },
    spsLastName: {
       type: String
    },
    isOwnerAlive: {
        type: Boolean
    },
    isSpsOwnerAlive: {
        type: Boolean
    },
    isActive: {
        type: Boolean,
        default: true
    }
});


const TitleOwner = mongoose.model("TitleOwner", titleOwnerSchema);
export default TitleOwner;