//const mongoose = require("mongoose");
import mongoose from "mongoose";

const titleSchema = new mongoose.Schema({
    titleNumber: {
        type: String,
        unique: [true, "Title Number already exist"]
    },
    lotNumber: {
        type: String
    },
    titleOwnerId: {
        type: Array
        },
    lotId: {
        type: String
    },
    isActive: {
        type: String,
        default: true
    }
});


const Title = mongoose.model("Title", titleSchema);
export default Title;
