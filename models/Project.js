import mongoose from "mongoose";

const projectSchema = new mongoose.Schema({
    
    projectName: {
        type: String
    },
    projectId: {
        type: String
    },
    isActive: {
        type: Boolean,
        default: true
    }
});

const Project = mongoose.model("Project", projectSchema);
export default Project;
