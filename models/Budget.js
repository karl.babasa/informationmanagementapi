//const mongoose = require("mongoose");
import mongoose from "mongoose";

const budgetSchema = new mongoose.Schema({
    budgetAmount: {
        type: Number,
        required: [true, "Budget Amount is Required"]
    },
    budgetName: {
        type: String
    },
    fiscalYearOfAllotment: {
        type: String
    },
    projectId: {
        type: String
    },
    budgetFund: {
        type: String
    },
    isActive: {
        type: Boolean,
        default: true
    }
});

const Budget = mongoose.model("Budget", budgetSchema);
export default Budget;
