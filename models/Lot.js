//const mongoose = require("mongoose");
import mongoose from "mongoose";

const lotSchema = new mongoose.Schema({
    lotNumber: {
        type: String,
        required: [true, "Lot Number is Required"]
    },
    lotArea: {
        type: Number
    },
    lotAreaAffected: {
        type: Number
    },
    pricePerSqm: {
        type: Number
    },
    lotTotalCompesation: {
        type: Number
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    isActive: {
        type: Boolean,
        default: false
    }
});


const Lot = mongoose.model("Lot", lotSchema);
export default Lot;
