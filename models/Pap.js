//const mongoose = require("mongoose");
import mongoose from "mongoose";

const papSchema = new mongoose.Schema({

    firstName: {
        type: String,
        required: [true, "First Name is Required"]
    },
    middleName: {
        type: String
    },
    lastName: {
        type: String,
        required: [true, "Last Name is Required"]
    },
    extensionName: {
        type: String
    },
    civilStatus: {
        type: String
    },
    spsFirstName: {
        type: String
    },
    spsMiddleName: {
        type: String
    },
    spsLastName: {
        type: String
    },
    spsExtensionName: {
        type: String
    },
    houseNumberAddress: {
        type: String
    },
    subdivisionAddress: {
        type: String
    },
    barangayAddress: {
        type: String
    },
    municipalAddress: {
        type: String
    },
    provinceAddress: {
        type: String
    },
    tin: {
        type: String
    },
    spsTin: {
        type: String
    },
    contactNumber: {
        type: String
    },
    emailAddress: {
        type: String
    },
    idCard: {
        type: Array,
        default: []
    },
    spsCard: {
        type: Array,
        default: []
    },
    lotId: {
        type: String
    },
    isActive: {
        type: Boolean,
        default: true
    }
});


const Pap = mongoose.model("Pap", papSchema);
export default Pap;