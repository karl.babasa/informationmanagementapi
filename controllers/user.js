import User from '../models/User.js'
import CryptoJS from 'crypto-js';

//PUT
export const editUser = async (req, res) => {
    if(req.body.password) {
        req.body.password = CryptoJS.AES.encrypt(req.body.password, process.env.PASS_SEC).toString()      
    }

    try{
        const updatedUser = await User.findByIdAndUpdate(req.params.id, {
            $set: req.body
        },{new: true}
        );
        res.status(200).json(updatedUser)
    } catch (err) {
        res.status(500).json({message: `${err}`})
    }
}

export const activateUser = async (req, res) => {

    try{
        const updatedUser = await User.findByIdAndUpdate(req.params.id, {
            isAdminAssistant: true
        },{new: true}
        );
        res.status(200).json(updatedUser)
    } catch (err) {
        res.status(500).json({message: `${err}`})
    }
}

export const deactivateUser = async (req, res) => {

    try{
        const updatedUser = await User.findByIdAndUpdate(req.params.id, {
            isAdminAssistant: false
        },{new: true}
        );
        res.status(200).json(updatedUser)
    } catch (err) {
        res.status(500).json({message: `${err}`})
    }
}

//DELETE
export const deleteUser = async (req,res) => {
    try{
        await User.findByIdAndDelete(req.params.id)
        res.status(200).json({message: "User has been deleted..."})
    }catch (err){
        res.status(500).json({message: `No user found / ${err}`})
    }
}

//GET
export const getUser = async (req,res) => {
    try{
        const user = await User.findById(req.params.id)
        const { password, ...others} = user._doc;
        res.status(200).json({others})
    }catch (err){
        res.status(500).json({message: `No user found / ${err}`})
        console.log(err)
    }
}

export const getUsers = async (req,res) => {
    const query = req.query.new;
    try{
        const users = query ? await User.find().sort({ _id: -1}).limit(5) : await User.find()
        res.status(200).json(users)
    }catch (err){
        res.status(500).json({message: `No user found / ${err}`})
        console.log(err)
    }
}