import Pap from '../models/Pap.js'

//POST
export const createPap = async (req,res) => {
        console.log(req.body)
     const newPap = new Pap({

            firstName: req.body.firstName,
            middleName: req.body.middleName,
            lastName: req.body.lastName,
            extensionName: req.body.extensionName,
            civilStatus: req.body.civilStatus,
            spsFirstName: req.body.spsFirstName,
            spsMiddleName: req.body.spsMiddleName,
            spsLastName: req.body.lastName,
            spsExtensionname: req.body.spsExtensionname,
            houseNumberAddress: req.body.houseNumberAddress,
            subdivisionAddress: req.body.subdivisionAddress,
            barangayAddress: req.body.barangayAddress,
            municipalAddress: req.body.municipalAddress,
            provinceAddress: req.body.provinceAddress,
            idCard: req.body.idCard,
            spsIdCard: req.body.spsIdCard,
            tin: req.body.tin,
            contactNumber: req.body.contactNumber,
            emailAddress: req.body.emailAddress,
            lotId: req.body.lotId
    });
    try{
        const savedPap = await newPap.save()
        res.status(201).json(savedPap)
    } catch (err) {
        res.status(500).json(err)
    }
    
}

//GET
export const getPap = async (req,res) => {

    try{

        const lot = await Pap.findById(req.params.id)
        console.log(Pap)
        res.status(200).json(lot)
    }catch (err){
        res.status(500).json({message: `No Pap found / ${err}`})
        console.log(err)
    }
}

export const getPaps = async (req,res) => {
    const query = req.query.new;
    try{
        const paps = query ? await Pap.find().sort({ _id: -1}).limit(5) : await Pap.find()
        res.status(200).json(paps)
    }catch (err){
        res.status(500).json({message: `No Pap found / ${err}`})
        console.log(err)
    }
}

//PUT
export const editPap = async (req, res) => {

    try{
        const updatedPap = await Pap.findByIdAndUpdate(req.params.id, {
            $set: req.body
        },{new: true}
        );
        res.status(200).json(updatedPap)
    } catch (err) {
        res.status(500).json({message: `${err}`})
    }
}

export const activatePap = async (req, res) => {

    try{
        const updatedPap = await Pap.findByIdAndUpdate(req.params.id, {
            isActive: true
        },{new: true}
        );
        res.status(200).json(updatedPap)
    } catch (err) {
        res.status(500).json({message: `${err}`})
    }
}

export const deactivatePap = async (req, res) => {

    try{
        const updatedPap = await Pap.findByIdAndUpdate(req.params.id, {
            isActive: false
        },{new: true}
        );
        res.status(200).json(updatedPap)
    } catch (err) {
        res.status(500).json({message: `${err}`})
    }
}

//DELETE
export const deletePap = async (req,res) => {
    try{
        await Pap.findByIdAndDelete(req.params.id)
        res.status(200).json({message: "Pap has been deleted..."})
    }catch (err){
        res.status(500).json({message: `No user found / ${err}`})
    }
}