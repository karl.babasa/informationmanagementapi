import Lot from '../models/Lot.js'

//POST
export const createLot = async (req,res) => {

    const newLot = new Lot({
        lotNumber: req.body.lotNumber,
        lotArea: req.body.lotArea,
        lotAreaAffected: req.body.lotAreaAffected,
        pricePerSqm: req.body.pricePerSqm,
        lotTotalCompesation: req.body.lotAreaAffected * req.body.pricePerSqm
    });
    try{
        const savedLot = await newLot.save()
        res.status(201).json(savedLot)
    } catch (err) {
        res.status(500).json(err)
    }
    
}

//GET
export const getLot = async (req,res) => {
    try{
        const lot = await Lot.findById(req.params.id)
        res.status(200).json(lot)
    }catch (err){
        res.status(500).json({message: `No Lot found / ${err}`})
        console.log(err)
    }
}

export const getLots = async (req,res) => {
    const query = req.query.new;
    try{
        const lots = query ? await Lot.find().sort({ _id: -1}).limit(5) : await Lot.find()
        res.status(200).json(lots)
    }catch (err){
        res.status(500).json({message: `No lot found / ${err}`})
        console.log(err)
    }
}

//PUT
export const editLot = async (req, res) => {

    try{
        const updatedLot = await Lot.findByIdAndUpdate(req.params.id, {
            $set: req.body
        },{new: true}
        );
        res.status(200).json(updatedLot)
    } catch (err) {
        res.status(500).json({message: `${err}`})
    }
}

export const activateLot = async (req, res) => {

    try{
        const updatedLot = await Lot.findByIdAndUpdate(req.params.id, {
            isActive: true
        },{new: true}
        );
        res.status(200).json(updatedLot)
    } catch (err) {
        res.status(500).json({message: `${err}`})
    }
}

export const deactivateLot = async (req, res) => {

    try{
        const updatedLot = await Lot.findByIdAndUpdate(req.params.id, {
            isActive: false
        },{new: true}
        );
        res.status(200).json(updatedLot)
    } catch (err) {
        res.status(500).json({message: `${err}`})
    }
}

//DELETE
export const deleteLot = async (req,res) => {
    try{
        await Lot.findByIdAndDelete(req.params.id)
        res.status(200).json({message: "Lot has been deleted..."})
    }catch (err){
        res.status(500).json({message: `No user found / ${err}`})
    }
}