import Budget from '../models/Budget.js'

//POST
export const createBudget = async (req,res) => {

    const newBudget = new Budget({
        
        budgetAmount: req.body.budgetAmount,
        budgetName: req.body.budgetName,
        fiscalYearOfAllotment: req.body.fiscalYearOfAllotment,
        projectId: req.body.projectId,
        budgetFund: req.body.budgetFund

    });
    try{
        const savedBudget = await newBudget.save()
        res.status(201).json(savedBudget)
    } catch (err) {
        res.status(500).json(err)
    }
    
}

//GET
export const getBudget = async (req,res) => {
    try{
        const budget = await Budget.findById(req.params.id)
        res.status(200).json(budget)
    }catch (err){
        res.status(500).json({message: `No Budget found / ${err}`})
        console.log(err)
    }
}

export const getBudgets = async (req,res) => {
    const query = req.query.new;
    try{
        const budgets = query ? await Budget.find().sort({ _id: -1}).limit(5) : await Budget.find()
        res.status(200).json(budgets)
    }catch (err){
        res.status(500).json({message: `No Budget found / ${err}`})
        console.log(err)
    }
}

//PUT
export const editBudget = async (req, res) => {

    try{
        const updatedBudget = await Budget.findByIdAndUpdate(req.params.id, {
            $set: req.body
        },{new: true}
        );
        res.status(200).json(updatedBudget)
    } catch (err) {
        res.status(500).json({message: `${err}`})
    }
}

export const activateBudget = async (req, res) => {

    try{
        const updatedBudget = await Budget.findByIdAndUpdate(req.params.id, {
            isActive: true
        },{new: true}
        );
        res.status(200).json(updatedBudget)
    } catch (err) {
        res.status(500).json({message: `${err}`})
    }
}

export const deactivateBudget = async (req, res) => {

    try{
        const updatedBudget = await Budget.findByIdAndUpdate(req.params.id, {
            isActive: false
        },{new: true}
        );
        res.status(200).json(updatedBudget)
    } catch (err) {
        res.status(500).json({message: `${err}`})
    }
}

//DELETE
export const deleteBudget = async (req,res) => {
    try{
        await Budget.findByIdAndDelete(req.params.id)
        res.status(200).json({message: "Budget has been deleted..."})
    }catch (err){
        res.status(500).json({message: `No Budget found / ${err}`})
    }
}