import User from '../models/User.js'
import CryptoJS from 'crypto-js';
import jwt from 'jsonwebtoken'

//REGISTER
export const createUser = async (req,res) => {

    const newUser = new User({
        username: req.body.username,
        password: CryptoJS.AES.encrypt(req.body.password, process.env.PASS_SEC).toString(),
        firstName: req.body.firstName,
        middleName: req.body.middleName,
        lastName: req.body.lastName,
        extensionName: req.body.extensionName
    });
    try{
        const savedUser = await newUser.save()
        res.status(201).json(savedUser)
    } catch (err) {
        res.status(500).json(err)
    }
    
}

//LOGIN
export const loginUser = async (req,res) => {
    try{
        const user = await User.findOne({username: req.body.username});

        if (!user) {
            return res.status(401).json({message: "Username does not exist"})
        }
    
        const hashedPassword = CryptoJS.AES.decrypt(user.password, process.env.PASS_SEC);

        const originalPassword = hashedPassword.toString(CryptoJS.enc.Utf8);

        if (originalPassword !== req.body.password) {
            return res.status(401).json({message: "Password Incorrect"})
        }

        const accessToken = jwt.sign({
            id: user._id,
            isAdmin: user.isAdmin,
            isAdminAssistant: user.isAdminAssistant
        },
            process.env.JWT_SEC,
             {expiresIn:"3d"}
        );

        const {password, ...others} = user._doc;

        res.status(200).json({...others, accessToken})
    } catch (err) {
        res.status(500).json(err)
    }
}