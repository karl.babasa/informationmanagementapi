import TitleOwner from '../models/TitleOwner.js'

//POST
export const createTitleOwner = async (req,res) => {

    const newTitleOwner = new TitleOwner({
        
        isOrganization: req.body.isOrganization,
        organizationName: req.body.organizationName,
        firstName: req.body.firstName,
        middleName: req.body.middleName,
        lastName: req.body.lastName,
        civilStatus: req.body.civilStatus,
        spsFirstName: req.body.spsFirstName,
        spsMiddleName: req.body.spsMiddleName,
        spsLastName: req.body.spsLastName,
        isOwnerAlive: req.body.isOwnerAlive,
        isSpsOwnerAlive: req.body.isSpsOwnerAlive

    });
    try{
        const savedTitleOwner = await newTitleOwner.save()
        res.status(201).json(savedTitleOwner)
    } catch (err) {
        res.status(500).json(err)
    }
    
}

//GET
export const getTitleOwner = async (req,res) => {
    try{
        const titleOwner = await TitleOwner.findById(req.params.id)
        res.status(200).json(titleOwner)
    }catch (err){
        res.status(500).json({message: `No Title Owner found / ${err}`})
        console.log(err)
    }
}

export const getTitleOwners = async (req,res) => {
    const query = req.query.new;
    try{
        const titleOwners = query ? await TitleOwner.find().sort({ _id: -1}).limit(5) : await TitleOwner.find()
        res.status(200).json(titleOwners)
    }catch (err){
        res.status(500).json({message: `No Title Owner found / ${err}`})
        console.log(err)
    }
}

//PUT
export const editTitleOwner = async (req, res) => {

    try{
        const updatedTitleOwner = await TitleOwner.findByIdAndUpdate(req.params.id, {
            $set: req.body
        },{new: true}
        );
        res.status(200).json(updatedTitleOwner)
    } catch (err) {
        res.status(500).json({message: `${err}`})
    }
}

export const activateTitleOwner = async (req, res) => {

    try{
        const updatedTitleOwner = await TitleOwner.findByIdAndUpdate(req.params.id, {
            isActive: true
        },{new: true}
        );
        res.status(200).json(updatedTitleOwner)
    } catch (err) {
        res.status(500).json({message: `${err}`})
    }
}

export const deactivateTitleOwner = async (req, res) => {

    try{
        const updatedTitleOwner = await TitleOwner.findByIdAndUpdate(req.params.id, {
            isActive: false
        },{new: true}
        );
        res.status(200).json(updatedTitleOwner)
    } catch (err) {
        res.status(500).json({message: `${err}`})
    }
}

//DELETE
export const deleteTitleOwner = async (req,res) => {
    try{
        await TitleOwner.findByIdAndDelete(req.params.id)
        res.status(200).json({message: "Title Owner has been deleted..."})
    }catch (err){
        res.status(500).json({message: `No Title Owner found / ${err}`})
    }
}