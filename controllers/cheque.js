import Cheque from '../models/Cheque.js'

//POST
export const createCheque = async (req,res) => {

    const newCheque = new Cheque({
        
        chequeType: req.body.chequeType,
        chequeNumber: req.body.chequeNumber,
        chequeAmount: req.body.chequeAmount,
        dateCreated: req.body.dateCreated,
        dateReleased: req.body.dateRelease,
        chequeName: req.body.chequeName,
        chequeStatus: req.body.chequeStatus,
        chequeRemark: req.body.chequeRemark,
        lotId: req.body.lotId

    });
    try{
        const savedCheque = await newCheque.save()
        res.status(201).json(savedCheque)
    } catch (err) {
        res.status(500).json(err)
    }
    
}

//GET
export const getCheque = async (req,res) => {
    try{
        const cheque = await Cheque.findById(req.params.id)
        res.status(200).json(cheque)
    }catch (err){
        res.status(500).json({message: `No Cheque found / ${err}`})
        console.log(err)
    }
}

export const getCheques = async (req,res) => {
    const query = req.query.new;
    try{
        const cheques = query ? await Cheque.find().sort({ _id: -1}).limit(5) : await Cheque.find()
        res.status(200).json(cheques)
    }catch (err){
        res.status(500).json({message: `No Cheque found / ${err}`})
        console.log(err)
    }
}

//PUT
export const editCheque = async (req, res) => {

    try{
        const updatedCheque = await Cheque.findByIdAndUpdate(req.params.id, {
            $set: req.body
        },{new: true}
        );
        res.status(200).json(updatedCheque)
    } catch (err) {
        res.status(500).json({message: `${err}`})
    }
}

export const activateCheque = async (req, res) => {

    try{
        const updatedCheque = await Cheque.findByIdAndUpdate(req.params.id, {
            isActive: true
        },{new: true}
        );
        res.status(200).json(updatedCheque)
    } catch (err) {
        res.status(500).json({message: `${err}`})
    }
}

export const deactivateCheque = async (req, res) => {

    try{
        const updatedCheque = await Cheque.findByIdAndUpdate(req.params.id, {
            isActive: false
        },{new: true}
        );
        res.status(200).json(updatedCheque)
    } catch (err) {
        res.status(500).json({message: `${err}`})
    }
}

//DELETE
export const deleteCheque = async (req,res) => {
    try{
        await Cheque.findByIdAndDelete(req.params.id)
        res.status(200).json({message: "Cheque has been deleted..."})
    }catch (err){
        res.status(500).json({message: `No Cheque found / ${err}`})
    }
}