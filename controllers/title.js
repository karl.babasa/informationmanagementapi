import Title from '../models/Title.js'

//POST
export const createTitle = async (req,res) => {

    const newTitle = new Title({
        titleNumber: req.body.titleNumber,
        lotArea: req.body.lotArea,
        lotNumber: req.body.lotNumber,
        titleOwnerId: req.body.titleOwnerId,
        lotId: req.body.lotId
    });
    try{
        const savedTitle = await newTitle.save()
        res.status(201).json(savedTitle)
    } catch (err) {
        res.status(500).json(err)
    }
    
}

//GET
export const getTitle = async (req,res) => {
    try{
        const title = await Title.findById(req.params.id)
        res.status(200).json(title)
    }catch (err){
        res.status(500).json({message: `No Title found / ${err}`})
        console.log(err)
    }
}

export const getTitles = async (req,res) => {
    const query = req.query.new;
    try{
        const titles = query ? await Title.find().sort({ _id: -1}).limit(5) : await Title.find()
        res.status(200).json(titles)
    }catch (err){
        res.status(500).json({message: `No Title found / ${err}`})
        console.log(err)
    }
}

//PUT
export const editTitle = async (req, res) => {

    try{
        const updatedTitle = await Title.findByIdAndUpdate(req.params.id, {
            $set: req.body
        },{new: true}
        );
        res.status(200).json(updatedTitle)
    } catch (err) {
        res.status(500).json({message: `${err}`})
    }
}

export const activateTitle = async (req, res) => {

    try{
        const updatedTitle = await Title.findByIdAndUpdate(req.params.id, {
            isActive: true
        },{new: true}
        );
        res.status(200).json(updatedTitle)
    } catch (err) {
        res.status(500).json({message: `${err}`})
    }
}

export const deactivateTitle = async (req, res) => {

    try{
        const updatedTitle = await Title.findByIdAndUpdate(req.params.id, {
            isActive: false
        },{new: true}
        );
        res.status(200).json(updatedTitle)
    } catch (err) {
        res.status(500).json({message: `${err}`})
    }
}

//DELETE
export const deleteTitle = async (req,res) => {
    try{
        await Title.findByIdAndDelete(req.params.id)
        res.status(200).json({message: "Title has been deleted..."})
    }catch (err){
        res.status(500).json({message: `No Title found / ${err}`})
    }
}