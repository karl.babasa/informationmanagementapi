import Project from '../models/Project.js'

//POST
export const createProject = async (req,res) => {

    const newProject = new Project({
        
        projectName: req.body.projectName,
        projectId: req.body.projectId

    });
    try{
        const savedProject = await newProject.save()
        res.status(201).json(savedProject)
    } catch (err) {
        res.status(500).json(err)
    }
    
}

//GET
export const getProject = async (req,res) => {
    try{
        const project = await Project.findById(req.params.id)
        res.status(200).json(project)
    }catch (err){
        res.status(500).json({message: `No Project found / ${err}`})
        console.log(err)
    }
}

export const getProjects = async (req,res) => {
    const query = req.query.new;
    try{
        const projects = query ? await Project.find().sort({ _id: -1}).limit(5) : await Project.find()
        res.status(200).json(projects)
    }catch (err){
        res.status(500).json({message: `No Project found / ${err}`})
        console.log(err)
    }
}

//PUT
export const editProject = async (req, res) => {

    try{
        const updatedProject = await Project.findByIdAndUpdate(req.params.id, {
            $set: req.body
        },{new: true}
        );
        res.status(200).json(updatedProject)
    } catch (err) {
        res.status(500).json({message: `${err}`})
    }
}

export const activateProject = async (req, res) => {

    try{
        const updatedProject = await Project.findByIdAndUpdate(req.params.id, {
            isActive: true
        },{new: true}
        );
        res.status(200).json(updatedProject)
    } catch (err) {
        res.status(500).json({message: `${err}`})
    }
}

export const deactivateProject = async (req, res) => {

    try{
        const updatedProject = await Project.findByIdAndUpdate(req.params.id, {
            isActive: false
        },{new: true}
        );
        res.status(200).json(updatedProject)
    } catch (err) {
        res.status(500).json({message: `${err}`})
    }
}

//DELETE
export const deleteProject = async (req,res) => {
    try{
        await Project.findByIdAndDelete(req.params.id)
        res.status(200).json({message: "Project has been deleted..."})
    }catch (err){
        res.status(500).json({message: `No Project found / ${err}`})
    }
}