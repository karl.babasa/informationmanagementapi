import express from 'express';
const app = express();
import mongoose from 'mongoose';
import dotenv from 'dotenv';
import lotRoute from './routes/lot.js';
import authRoute from './routes/auth.js';
import userRoute from './routes/user.js';
import papRoute from './routes/pap.js';
import titleRoute from './routes/title.js';
import titleOwnerRoute from './routes/titleOwner.js';
import chequeRoute from './routes/cheque.js';
import budgetRoute from './routes/budget.js';
import projectRoute from './routes/project.js';

dotenv.config()

mongoose.connect(process.env.MONGO_URL).then(() => console.log("Connected to Database")).catch((err) => {
    console.log(err);
});
app.use(express.json())

app.use("/lot", lotRoute);
app.use("/auth", authRoute);
app.use("/user", userRoute);
app.use("/pap", papRoute);
app.use("/title", titleRoute);
app.use("/titleOwner", titleOwnerRoute);
app.use("/cheque", chequeRoute);
app.use("/budget", budgetRoute);
app.use("/project", projectRoute);

app.listen(process.env.PORT || 5000, () => {
    console.log("Backend Server is Running!")
});